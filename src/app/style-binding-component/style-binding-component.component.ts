import { Component } from '@angular/core';

@Component({
  selector: 'app-style-binding-component',
  templateUrl: './style-binding-component.component.html',
  styleUrls: ['./style-binding-component.component.css']
})
export class StyleBindingComponentComponent {
 Style={
  'color':'red',
  'background':'green'
 }
 changecolor(){
  this.Style.background='black'
  this.Style.color='white'
 }
 previouscolor(){
  this.Style.background='green'
  this.Style.color='red'
 }
 classpermission:boolean=true
 classpermission2:boolean=false

}
