import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StyleBindingComponentComponent } from './style-binding-component.component';

describe('StyleBindingComponentComponent', () => {
  let component: StyleBindingComponentComponent;
  let fixture: ComponentFixture<StyleBindingComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StyleBindingComponentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StyleBindingComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
