import { Component } from '@angular/core';

@Component({
  selector: 'app-directives-component',
  templateUrl: './directives-component.component.html',
  styleUrls: ['./directives-component.component.css']
})
export class DirectivesComponentComponent {

  // ngif first example start here 

  show=true
  hideifblock(){
    this.show=false
  }
  showifblock(){
    this.show=true
  }
// ngif first example end here
// ngif second example start here -->
  show2:string='yes'
// ngif second example end here -->

// ngfor first example start here -->
 users=['sanjay','rahul','jony','pritam']
// ngfor first example end here -->

// ngfor second example start here -->
userDetails=[
  {name:'Anil',email:'s@gmail.com'},
  {name:'Sahil',email:'sahil@gmail.com'},
  {name:'Mukesh',email:'M@gmail.com'}
]
// ngfor second example end here -->



}
