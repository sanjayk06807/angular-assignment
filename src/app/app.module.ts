import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { DataBindingComponentComponent } from './data-binding-component/data-binding-component.component';

import { StyleBindingComponentComponent } from './style-binding-component/style-binding-component.component';
import { DirectivesComponentComponent } from './directives-component/directives-component.component';

@NgModule({
  declarations: [
    AppComponent,
    DataBindingComponentComponent,
    StyleBindingComponentComponent,
    DirectivesComponentComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
